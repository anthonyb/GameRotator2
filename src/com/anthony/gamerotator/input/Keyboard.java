package com.anthony.gamerotator.input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {
	
	public enum Key {
		
		W(KeyEvent.VK_W),
		A(KeyEvent.VK_A),
		S(KeyEvent.VK_S),
		D(KeyEvent.VK_D),
		ENTER(KeyEvent.VK_ENTER)
		;
		
		private int keyCode;
		
		private boolean pressed;
		
		private Key(int keyCode) {
			
			this.keyCode = keyCode;
			
			this.pressed = false;
		}
		
		public void setPressed(boolean pressed) {
			
			this.pressed = pressed;
		}
		
		public int getKeyCode() {
			
			return this.keyCode;
		}
		
		public boolean getPressed() {
			
			return this.pressed;
		}
	}
	
	@Override
	public void keyTyped(KeyEvent e) {}
	
	@Override
	public void keyPressed(KeyEvent e) {
		
		int keyCode = e.getKeyCode();
		
		for (Key key : Key.values()) {
			
			if (keyCode == key.getKeyCode()) {
				
				key.setPressed(true);
				return;
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		
		int keyCode = e.getKeyCode();
		
		for (Key key : Key.values()) {
			
			if (keyCode == key.getKeyCode()) {
				
				key.setPressed(false);
				return;
			}
		}
	}
}
