package com.anthony.gamerotator.player;

import com.anthony.gamerotator.GameRotator;
import com.anthony.gamerotator.input.Keyboard.Key;

import java.awt.*;

public class Player {
	
	private static Color LIGHT_BLUE = new Color(0xFF99CCFF);
	
	private int speed;
	
	private GameRotator instance;
	
	private Rectangle rect;
	
	public Player(GameRotator instance) {
		
		this.instance = instance;
		
		this.speed = 1;
		
		this.rect = new Rectangle(240, 240, 20, 20);
	}
	
//	Methods
	
	public void render(Graphics g) {
		
		if (this.instance.getColorMode()) {
			
			g.setColor(LIGHT_BLUE);
		} else {
			
			g.setColor(Color.PINK);
		}		
		
		g.fillRect(this.rect.x, this.rect.y, this.rect.width, this.rect.height);
	}
	
	public void tick() {
		
		if (Key.W.getPressed()) {
			
			if (this.rect.getMinY() > 0) {
				
				this.rect.y -= this.speed;
			}
		}
		if (Key.S.getPressed()) {
			
			if (this.rect.getMaxY() < 500) {
				
				this.rect.y += this.speed;
			}
		}
		if (Key.A.getPressed()) {
			
			if (this.rect.getMinX() > 0) {
				
				this.rect.x -= this.speed;
			}
		}
		if (Key.D.getPressed()) {
			
			if (this.rect.getMaxX() < 500) {
				
				this.rect.x += this.speed;
			}
		}
	}
	
//	Setters
	
	public void setSpeed(int speed) {
		
		this.speed = speed;
	}
	
//	Getters
	
	public int getSpeed() {
		
		return this.speed;
	}
	
	public Rectangle getRect() {
		
		return this.rect;
	}
}
