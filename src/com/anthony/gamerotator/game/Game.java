package com.anthony.gamerotator.game;

import com.anthony.gamerotator.GameRotator;

import java.awt.*;

public abstract class Game {
	
	public enum Result {
		
		IN_GAME(),
		FAILED(),
		COMPLETED()
		;
		
		private Result() {}
	}
	
	protected static final Font
	
		TITLE_FONT = new Font("Times New Roman", 1, 50),
		SUBTITLE_FONT = new Font("Times New Roman", 2, 25),
		MENU_OPTIONS_FONT = new Font("Book Antiqua", 0, 30),
		VERDANA_12_PT = new Font("Verdana", 1, 12)
	;
	
	protected GameRotator instance;

	private static final Color LIGHT_GREEN = new Color(0xFF66CC99);
	
	private int titleTimer, subTitleTimer;
	
	private String gameTitle, title, subTitle;
	
	private Result result;
	
	/**
	 * Constructor for a new Game.
	 * @param instance GameRotator instance.
	 * @param gameTitle The name of the Game.
	 */
	public Game(GameRotator instance, String gameTitle) {
		
		this.instance = instance;
		
		this.titleTimer = 0;
		this.subTitleTimer = 0;
		
		this.setGameTitle(gameTitle);
		this.title = "";
		this.subTitle = "";
		
		this.result = Result.IN_GAME;
	}
	
//	Methods
	
	public abstract void tick();
	public abstract void render(Graphics g);
	
	protected void tickTitle() {
		
		if (this.titleTimer > 0) {
			
			this.titleTimer--;
		}
		
		if (this.subTitleTimer > 0) {
			
			this.subTitleTimer--;
		}
	}
	
	protected void renderTitles(Graphics g) {
		
		if (this.titleTimer != 0 || this.subTitleTimer != 0) {
			
			if (this.instance.getColorMode()) {
				
				g.setColor(Color.YELLOW);
			} else {
				
				g.setColor(Game.LIGHT_GREEN);
			}
			
			if (this.titleTimer != 0) {
				
				g.setFont(Game.TITLE_FONT);
				
				
				this.instance.drawCenteredString(this.title, 500, 100, g);
			}
			
			if (this.subTitleTimer != 0) {
				
				g.setFont(Game.SUBTITLE_FONT);
				
				this.instance.drawCenteredString(this.subTitle, 500, 150, g);
			}
		}
	}
	
	protected void renderBackground(Graphics g) {
		
		if (this.instance.getColorMode()) {
			
			g.setColor(Color.BLACK);
		} else {
			
			g.setColor(Color.WHITE);
		}		
		
		g.fillRect(0, 0, 500, 500);
	}
	
//	Setters
	
	protected void setResult(Result result) {
		
		this.result = result;
	}
	
	protected void setGameTitle(String title) {
		
		this.gameTitle = title;
		this.instance.getJFrame().setTitle(GameRotator.TITLE + " - " + title);
	}
	
	protected void setTitle(String title, int seconds) {
		
		this.title = "[" + title + "]";
		this.titleTimer = seconds * 125; //125 is ticks per second
	}
	
	protected void setSubTitle(String subTitle, int seconds) {
		
		this.subTitle = subTitle;
		this.subTitleTimer = seconds * 125;
	}
	
//	Getters
	
	public Result getResult() {
		
		return this.result;
	}
	
	public String getGameTitle() {
		
		return this.gameTitle;
	}
	
	protected String getTitle() {
		
		return this.title;
	}
	
	protected String getSubTitle() {
		
		return this.subTitle;
	}
}
