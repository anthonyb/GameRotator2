package com.anthony.gamerotator.game.menus;

import com.anthony.gamerotator.GameRotator;
import com.anthony.gamerotator.game.Game;
import com.anthony.gamerotator.input.Keyboard.Key;

import java.awt.*;

public class Failed extends Game {
	
	private int selected, upTimer, downTimer, loadScreenTimer;
	
	private String[] options;
	
	public Failed(GameRotator instance, String levelName) {
		
		super(instance, "Failed");
		
		super.setTitle("Failed", -1);
		super.setSubTitle("You failed the level " + levelName + "!", -1);
		
		super.instance.setColorMode(false);
		
		this.selected = 0;
		this.upTimer = 0;
		this.downTimer = 0;
		this.loadScreenTimer = 62; //About half a second
		
		this.options = new String[]{"Play Again", "About", "Quit"};
	}
	
	@Override
	public void tick() {
		
		if (this.loadScreenTimer > 0) {
			
			this.loadScreenTimer--;
			return;
		}
		
		if (this.upTimer > 0) this.upTimer--;
		if (this.downTimer > 0) this.downTimer--;
		
		if (this.upTimer == 0 && Key.W.getPressed()) {
			
			if (this.selected > 0) {
				
				this.selected--;
				this.upTimer = 20;
			}
		}
		
		if (this.downTimer == 0 && Key.S.getPressed()) {
			
			if (this.selected < this.options.length - 1) {
				
				this.selected++; 
				this.downTimer = 20;
			}
		}
		
		if (Key.ENTER.getPressed()) {
			
			switch (this.selected) {
			case 0:
				
				super.instance.setRunning(false);
				super.instance.run();
				break;
			case 1:
				
				super.instance.setGame(new About(super.instance));
				break;
			case 2:
				
				System.exit(0);
				break;
			}
		}
	}

	@Override
	public void render(Graphics g) {
		
		super.renderBackground(g);
		
		super.renderTitles(g);
		
		g.setFont(Game.MENU_OPTIONS_FONT);
		
		for (int i = 0; i < this.options.length; i++) {
			
			String option = this.options[i];
			
			if (this.selected == i) option = "> " + option + " <";
			
			super.instance.drawCenteredString(option, 500, 350 + (i * 30), g);
		}
	}
}
