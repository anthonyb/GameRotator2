package com.anthony.gamerotator.game.menus;

import com.anthony.gamerotator.GameRotator;
import com.anthony.gamerotator.game.Game;
import com.anthony.gamerotator.input.Keyboard.Key;

import java.awt.*;

public class MainMenu extends Game {
	
	private int selected, upTimer, downTimer;
	
	private String[] options;
	
	public MainMenu(GameRotator instance) {
		
		super(instance, "Main Menu");
		
		super.setTitle(GameRotator.TITLE, -1);
		super.setSubTitle("Made by Anthony Bhasin", -1);
		
		this.selected = 0;
		this.upTimer = 0;
		this.downTimer = 0;
		
		this.options = new String[]{"Play", "About", "Quit"};
	}

	@Override
	public void tick() {
		
		if (this.upTimer > 0) this.upTimer--;
		if (this.downTimer > 0) this.downTimer--;
		
		if (this.upTimer == 0 && Key.W.getPressed()) {
			
			if (this.selected > 0) {
				
				this.selected--;
				this.upTimer = 20;
			}
		}
		
		if (this.downTimer == 0 && Key.S.getPressed()) {
			
			if (this.selected < this.options.length - 1) {
				
				this.selected++; 
				this.downTimer = 20;
			}
		}
		
		if (Key.ENTER.getPressed()) {
			
			switch (this.selected) {
			case 0:
				
				super.instance.setRunning(false);
				super.instance.run();
				break;
			case 1:
				
				super.instance.setGame(new About(super.instance));
				break;
			case 2:
				
				System.exit(0);
				break;
			}
		}
	}

	@Override
	public void render(Graphics g) {
		
		super.renderBackground(g);
		
		super.renderTitles(g);
		
		g.setFont(Game.MENU_OPTIONS_FONT);
		
		for (int i = 0; i < this.options.length; i++) {
			
			String option = this.options[i];
			
			if (this.selected == i) option = "> " + option + " <";
			
			super.instance.drawCenteredString(option, 500, 350 + (i * 30), g);
		}
	}
}
