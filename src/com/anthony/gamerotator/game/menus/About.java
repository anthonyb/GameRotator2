package com.anthony.gamerotator.game.menus;

import com.anthony.gamerotator.GameRotator;
import com.anthony.gamerotator.game.Game;
import com.anthony.gamerotator.input.Keyboard.Key;

import java.awt.*;

public class About extends Game {
	
	private int selected, upTimer, downTimer, loadScreenTimer;
	
	private String[] options;
	
	public About(GameRotator instance) {
		
		super(instance, "About");
		
		super.setTitle("About", -1);
		super.setSubTitle(GameRotator.TITLE, -1);
		
		super.instance.setColorMode(false);
		
		this.selected = 0;
		this.upTimer = 0;
		this.downTimer = 0;
		this.loadScreenTimer = 62; //About half a second
		
		this.options = new String[]{"Play", "Quit"};
	}

	@Override
	public void tick() {
		
		if (this.loadScreenTimer > 0) {
			
			this.loadScreenTimer--;
			return;
		}
		
		if (this.upTimer > 0) this.upTimer--;
		if (this.downTimer > 0) this.downTimer--;
		
		if (this.upTimer == 0 && Key.W.getPressed()) {
			
			if (this.selected > 0) {
				
				this.selected--;
				this.upTimer = 20;
			}
		}
		
		if (this.downTimer == 0 && Key.S.getPressed()) {
			
			if (this.selected < this.options.length - 1) {
				
				this.selected++; 
				this.downTimer = 20;
			}
		}
		
		if (Key.ENTER.getPressed()) {
			
			switch (this.selected) {
			case 0:
				
				super.instance.setRunning(false);
				super.instance.run();
				break;
			case 1:
				
				System.exit(0);
				break;
			}
		}
	}

	@Override
	public void render(Graphics g) {
		
		super.renderBackground(g);
		super.renderTitles(g);
		
		g.setFont(Game.VERDANA_12_PT);
		
		super.instance.drawCenteredString(GameRotator.TITLE + " is a rotating game", 500, 200, g);
		super.instance.drawCenteredString("cycler that picks a random game to play", 500, 220, g);
		super.instance.drawCenteredString("from a folder of .class files.", 500, 240, g);
		super.instance.drawCenteredString(GameRotator.TITLE + " was created by", 500, 280, g);
		super.instance.drawCenteredString("Anthony Bhasin from", 500, 300, g);
		super.instance.drawCenteredString("12/10/15 to 12/12/15!", 500, 320, g);
		
		g.setFont(Game.MENU_OPTIONS_FONT);
		
		for (int i = 0; i < this.options.length; i++) {
			
			String option = this.options[i];
			
			if (this.selected == i) option = "> " + option + " <";
			
			super.instance.drawCenteredString(option, 500, 400 + (i * 30), g);
		}
	}
}
