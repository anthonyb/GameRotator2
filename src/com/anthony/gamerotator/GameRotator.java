package com.anthony.gamerotator;

import com.anthony.gamerotator.game.Game;
import com.anthony.gamerotator.game.Game.Result;
import com.anthony.gamerotator.game.menus.Failed;
import com.anthony.gamerotator.game.menus.MainMenu;
import com.anthony.gamerotator.input.Keyboard;
import com.anthony.gamerotator.player.Player;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Random;

public class GameRotator {
	
	public static final String TITLE = "Game Rotator";
	
	private boolean running, colorMode;
	
	private String lastGameName;
	
	private JFrame frame;
	private Canvas canvas;
	
	private BufferStrategy bs;
	private Graphics g;
	
	private File gamesDir;
	
	private Random random;
	
	private Class<?>[] games;
	
	private Game currentGame;
	
	private Player player;
	
	public GameRotator() throws IOException {
		
		this.running = false;
		this.colorMode = false;
		
		this.frame = new JFrame();
		
		this.frame.setResizable(false);
		
		this.canvas = new Canvas();
		
		this.canvas.setSize(500, 500);
		this.canvas.addKeyListener(new Keyboard());
		
		this.frame.add(this.canvas);
		
		this.frame.pack();
		
		this.canvas.createBufferStrategy(3);
		
		this.frame.setTitle(GameRotator.TITLE);
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setLocationRelativeTo(null);
		this.frame.setVisible(true);
		
		this.frame.setIconImage(ImageIO.read(GameRotator.class.getResourceAsStream("/gr.png")));
		
		this.random = new Random();
		
		this.player = new Player(this);
		
		this.setupFiles();
		
		this.run();
	}
	
//	Methods

	public void run() {

		this.newGame();
		
		this.running = true;
		
		long lastTime = System.nanoTime();
		double nsPerTick = 1_000_000_000D / 125; //max ticks and frames
		
		long lastTimer = System.currentTimeMillis();
		double delta = 0;
		
		while (this.running) {
			
			long now = System.nanoTime();
			delta += (now - lastTime) / nsPerTick;
			lastTime = now;
			
			while (delta >= 1) {
				
				this.tick();
				this.render();
				
				delta--;
			}
			
			if (System.currentTimeMillis() - lastTimer >= 1_000) {
				
				lastTimer += 1_000;
			}
		}
	}
	
	public void drawCenteredString(String s, int w, int h, Graphics g) {
		
		FontMetrics fm = g.getFontMetrics();
		int x = (w - fm.stringWidth(s)) / 2;
		g.drawString(s, x, h);
	}
	
	private void setupFiles() {
		
		this.gamesDir = new File(GameRotator.TITLE + " Games");
		
		if (!this.gamesDir.exists()) {

//			TODO copy default "games" over (none yet made)
			this.gamesDir.mkdir();
		}
		
		if (!this.gamesDir.isDirectory()) {
			
			JOptionPane.showMessageDialog(null, GameRotator.TITLE + " could not load any games because " + this.gamesDir.getAbsolutePath() + " does not point to a directory!");
			System.exit(0);
			return;
		}
		
		
		File actualGamesLocation = new File(this.gamesDir.getAbsolutePath() + File.separator + "classes");
		
		if (!actualGamesLocation.exists()) {
			
			actualGamesLocation.mkdir();
		}
		
		if (!actualGamesLocation.isDirectory()) {
			
			JOptionPane.showMessageDialog(null, GameRotator.TITLE + " could not load any games because " + actualGamesLocation.getAbsolutePath() + " does not point to a directory!");
			System.exit(0);
			return;
		}
		
		this.games = new Class<?>[actualGamesLocation.listFiles().length];
		
		try {
			
			URL url = this.gamesDir.toURI().toURL();
			URL[] urls = new URL[]{url};
			
			URLClassLoader cl = new URLClassLoader(urls);
			
			File[] gameFiles = actualGamesLocation.listFiles();
			
			for (int i = 0; i < gameFiles.length; i++) {
				
				File f = gameFiles[i];
				
				Class<?> cls = cl.loadClass("classes." + f.getName().replace(".class", ""));
				
				if (cls.getSuperclass().equals(Game.class)) {
					
					this.games[i] = cls;
				}
			}
			
			if (this.games.length == 0) {
				
				JOptionPane.showMessageDialog(null, "There are no games in the folder:\n" + actualGamesLocation.getAbsolutePath() + "\nTry again after adding at least one game!");
				
				try {
					
					cl.close();
				} catch (IOException e) {
					
					e.printStackTrace();
				}
				
				System.exit(0);
				return;
			}
			
			if (this.games.length == 1) {
				
				JOptionPane.showMessageDialog(null, "Since there is only 1 game in the folder:\n" + actualGamesLocation.getAbsolutePath() + "\nThis will cause the same game to play repeatedly!");
			}
			
			try {
				
				cl.close();
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		} catch (MalformedURLException e) {
			
			JOptionPane.showMessageDialog(null, e.toString());
			System.exit(0);
			return;
		} catch (ClassNotFoundException e) {
			
			JOptionPane.showMessageDialog(null, e.toString());
			System.exit(0);
			return;
		}
	}
	
	private void newGame() {
		
		if (this.currentGame == null) {
			
			this.currentGame = new MainMenu(this);
			this.lastGameName = "";
			return;
		}
		
		try {
			
			int index = this.random.nextInt(this.games.length);
			
			Class<?> c = this.games[index];
			
			if (this.games.length > 1 && c.getName().equals(this.lastGameName)) {
				
				if (index == 0) {
					
					index++;
				} else {
					
					index--;
				}
			}
			
			c = this.games[index];
			
			this.lastGameName = c.getName();
			
			this.currentGame = (Game) c.getDeclaredConstructor(GameRotator.class).newInstance(this);
		} catch (InstantiationException e) {
			
			JOptionPane.showMessageDialog(null, e.toString());
			System.exit(0);
		} catch (IllegalAccessException e) {
			
			JOptionPane.showMessageDialog(null, e.toString());
			System.exit(0);
		} catch (IllegalArgumentException e) {
			
			JOptionPane.showMessageDialog(null, e.toString());
			System.exit(0);
		} catch (InvocationTargetException e) {
			
			JOptionPane.showMessageDialog(null, e.toString());
			System.exit(0);
		} catch (NoSuchMethodException e) {
			
			JOptionPane.showMessageDialog(null, e.toString());
			System.exit(0);
		} catch (SecurityException e) {
			
			JOptionPane.showMessageDialog(null, e.toString());
			System.exit(0);
		}
	}
	
	private void render() {
		
		this.bs = this.canvas.getBufferStrategy();
		this.g = this.bs.getDrawGraphics();
		
		this.g.fillRect(0, 0, 500, 500);
		
		this.currentGame.render(this.g);
		this.player.render(this.g); //Player renders last
		
		this.g.dispose();
		this.bs.show();
	}
	
	private void tick() {
		
		this.player.tick(); //Player updates first
		
		Result currentResult = this.currentGame.getResult();
		
		if (currentResult.equals(Result.IN_GAME)) {
			
			this.currentGame.tick();
		} else {
			
			if (currentResult.equals(Result.FAILED)) {
				
				this.currentGame = new Failed(this, this.currentGame.getGameTitle());
				return;
			} else if (currentResult.equals(Result.COMPLETED)) {
				
				this.newGame();
			}
		}
	}
	
//	Setters
	
	public void setRunning(boolean running) {
		
		this.running = running;
	}
	
	public void setColorMode(boolean colorMode) {
		
		this.colorMode = colorMode;
	}
	
	public void toggleColorMode() {
		
		this.colorMode = !this.colorMode;
	}
	
	public void setGame(Game game) {
		
		this.currentGame = game;
	}
	
//	Getters
	
	public boolean getColorMode() {
		
		return this.colorMode;
	}
	
	public JFrame getJFrame() {
		
		return this.frame;
	}
	
	public Player getPlayer() {
		
		return this.player;
	}
}
